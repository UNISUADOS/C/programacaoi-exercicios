#include <stdio.h>
#include <stdlib.h>

int summation_iteration(int number)
{
    int sum = 0;

    for(int i = 1; i <= number; i++)
    {
        sum += i;
    }

    return sum;
}

int summation_recursive(int number)
{
    if(number == 1)
    {
        return 1;
    }

    return number + summation_recursive(number - 1);
}

int main()
{
    int number;
    scanf("%d", &number);

    int result_iteration = summation_iteration(number);
    printf("Funcao iteratia: %d\n\n", result_iteration);

    int result_recursive = summation_recursive(number);
    printf("Funcao recursiva: %d\n\n", result_recursive);

    system("pause");
}