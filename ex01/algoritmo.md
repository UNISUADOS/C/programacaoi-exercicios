# Exercício 01 #

## Descrição ##

Faça um algoritmo e o programa para ler um número digitado pelo usuário, calcular e exibir o resultado da seguinte operação:

`S = n + (n-1) + (n-2) + ... + 1`

## Algoritmo ##

- a
- b

```
ler numero

variabel soma = 0

para atual de 1 ate numero faca:
    soma += atual

escreva(soma)
```

## Algoritmo usando função ##

```
funcao somarial(numero)
    soma = 0
    para atual de 1 ate numero faca
        soma += atual
    fimpara

    retornar soma

ler(numero)
resultado = somarial(numero)
imprima(resultado)
```

## Algoritmo usando função recursiva ##

```
funcao somarial(numero)
    se numero == 1
        retornar 1

    retornar numero + somarial(numero - 1)

ler(numero)
resultado = somarial(numero)
imprima(resultado)
```
